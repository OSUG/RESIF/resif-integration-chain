#!/usr/bin/env -S uv run
# /// script
# requires-python = ">=3.12, <3.13"
# dependencies = [
#     "pika>=1.3.2",
#     "resifdatatransfertransaction>=0.1.2",
#     "zabbix_utils>2",
# ]
# ///

import argparse
import logging
import os
import platform
import re
import shutil
import signal
import sys
import traceback
from functools import partial
from logging.handlers import WatchedFileHandler

import pika
from ResifDataTransferTransaction import Transaction
from zabbix_utils import ItemValue, Sender

# RESIF specific lib
from integration_worker_utils import launch_processing_module, ls_files

RUNMODE = os.environ.get("RUNMODE", "test")

AMQP_SERVER = os.environ.get("AMQP_SERVER", "resif-mq.u-ga.fr")
AMQP_PORT = os.environ.get("AMQP_PORT", 5672)
AMQP_VHOST = os.environ.get("RUNMODE", "dev")
AMQP_USER = os.environ.get("AMQP_USER", "integrator")
AMQP_PASSWORD = os.environ.get("AMQP_PASSWORD")
AMQP_HEARTBEAT = os.environ.get("AMQP_HEARTBEAT", 0)
AMQP_TIMEOUT = os.environ.get("AMQP_TIMEOUT", 300)

ZABBIX_CONF = os.getenv("ZABBIX_CONF", "/etc/zabbix/zabbix_agent2.conf")
ZABBIX_HOST = platform.node()
ZABBIX_KEY = "resif_integration_worker"

ROUTING_KEY_DEFAULT = os.environ.get("ROUTING_KEY_DEFAULT", "incoming")
ROUTING_KEY_LONG = os.environ.get("ROUTING_KEY_LONG", f"{ROUTING_KEY_DEFAULT}-long")
EXCHANGE = os.environ.get("EXCHANGE", "integration")

XML_STORE_PATH = os.getenv(
    "XML_STORE_PATH", "/mnt/nfs/summer/validated_seismic_metadata/transaction_xml/"
)
RSYNC_MODULES_DIR = os.getenv("RSYNC_MODULES_DIR", "/mnt/nfs/summer/incoming")
MAIN_DIR = os.getenv("MAIN_DIR", os.path.expanduser("~"))

LOG_DIR = os.path.join(MAIN_DIR, "logs")
TEMP_WORKING_DIR = os.path.join(MAIN_DIR, "work/temp")
MODULE_DIR = os.path.join(MAIN_DIR, "resif-integration-processing-modules")
TRANSACTION_ANALYSE_EXE = os.path.join(
    MAIN_DIR, "resif-transaction-analyse/ResifTransactionAnalyse.py"
)

PROCESSING_MODULES = {
    "seismic_data_miniseed": [
        MODULE_DIR + "/data_xml/processing-module-scanMseed.sh",
        MODULE_DIR + "/data_xml/processing-module-sds.sh",
        MODULE_DIR + "/data_xml/processing-module-demux.sh",
        MODULE_DIR + "/data_xml/processing-module-block4096.sh",
        MODULE_DIR + "/data_xml/processing-module-quality.sh",
        MODULE_DIR + "/data_xml/processing-module-contain.sh",
        MODULE_DIR + "/data_xml/processing-module-duration.sh",
        MODULE_DIR + "/data_xml/processing-module-encode.sh",
        MODULE_DIR + "/data_xml/processing-module-channel.sh",
        MODULE_DIR + "/data_xml/processing-module-archive.sh",
        MODULE_DIR + "/data_xml/processing-module-inventory.sh",
    ],
    "seismic_data_ph5": [
        MODULE_DIR + "/data_ph5/T0_check_files_format.py",
        MODULE_DIR + "/data_ph5/T0bis_check_only_one_network.py",
        MODULE_DIR + "/data_ph5/T1_network_is_in_inventory.py",
        MODULE_DIR + "/data_ph5/T2_all_stations_in_inventory.py",
        MODULE_DIR + "/data_ph5/T3_miniseed_conversion.py",
        MODULE_DIR + "/data_ph5/T4_rsync_to_archive.py",
        MODULE_DIR + "/data_ph5/T5_inventory.py",
    ],
    "metadata_stationxml": [
        MODULE_DIR + "/metadata_xml/0-processing-module-one-file-only.bash",
        MODULE_DIR + "/metadata_xml/1-processing-module-valid-stationXML-warnings.bash",
        MODULE_DIR + "/metadata_xml/2-processing-module-valid-stationXML.bash",
        MODULE_DIR + "/metadata_xml/3-processing-module-unique-station.bash",
        MODULE_DIR + "/metadata_xml/4-processing-module-declared-network.bash",
        MODULE_DIR + "/metadata_xml/5-processing-module-database-network.bash",
        MODULE_DIR + "/metadata_xml/6-processing-module-data-station-FR.bash",
        MODULE_DIR + "/metadata_xml/6b-processing-module-coloc-dates-consistency.bash",
        MODULE_DIR + "/metadata_xml/7-processing-module-to-transaction.bash",
        MODULE_DIR + "/metadata_xml/9-processing-module-to-resifinv.bash",
    ],
}


def log_setup(long):
    # pika log level
    logging.getLogger("pika").setLevel(logging.WARNING)

    # log to file
    logfile = os.path.join(LOG_DIR, "integrationworker")
    if long:
        logfile = logfile + "-long"
    logfile = logfile + ".log"

    # Configuration du logger
    # WatchedFileHandler permet de bien gérer les rotations de logs par logrotate
    log_handler = WatchedFileHandler(logfile)
    formatter = logging.Formatter(
        "%(asctime)s %(process)d %(levelname)s: %(message)s", "%Y-%m-%d %H:%M:%S"
    )
    log_handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    if RUNMODE == "production":
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.DEBUG)


def transaction_process(message):
    zabbix_packets = []
    try:
        try:
            (module, transaction_id) = message.split("/")
        except ValueError:
            logging.exception("invalid message format : aborting.")
            return
        if not re.match("[0-9A-Z]{8}", transaction_id):
            logging.error("invalid transactionID : %s", transaction_id)
        if not module or not transaction_id:
            logging.error(
                "invalid module name (%s) or transaction ID (%s) : aborting.",
                module,
                transaction_id,
            )
            return
        incoming_path = os.path.join(RSYNC_MODULES_DIR, module.lower())
        if not os.path.isdir(incoming_path):
            logging.error(
                "directory %s does not exist. Check that rsync module %s is valid.",
                incoming_path,
                module,
            )
            return

        basepath = os.path.join(incoming_path, transaction_id)
        logging.info("new transaction id=%s path=%s", transaction_id, basepath)

        if not os.path.isdir(basepath):
            logging.error(
                "directory not found: %s (remote client *may* have performed "
                "a test without actually transferring data. Aborting.",
                basepath,
            )
            return

        xmlfilename = f"{transaction_id}.xml"
        xmlfullpath = os.path.join(basepath, xmlfilename)

        if not os.path.isfile(xmlfullpath):
            logging.error("XML file %s not found, aborting.", xmlfullpath)
            return

        # create a temporary working directory
        tempdir = os.path.join(TEMP_WORKING_DIR, transaction_id)
        os.mkdir(tempdir)
        logging.info("temporary working directory created: %s", tempdir)

        # make a backup of original XML file
        shutil.copy(xmlfullpath, tempdir)

        # move XML to store
        targetxml = os.path.join(XML_STORE_PATH, f"{transaction_id}.xml")
        logging.info("moving %s to %s", xmlfilename, targetxml)
        shutil.move(xmlfullpath, targetxml)

        # load XML
        tree = Transaction.Transaction(targetxml)

        # get incoming files list, update XML
        allfiles = ls_files(basepath)
        tree.set_filelist(allfiles)
        tree.set_status("4")
        tree.set_comment(
            "Data received on datacentre side, data integration steps in progress."
        )
        tree.write(targetxml)
        logging.info("%d files to be processed", len(allfiles))

        # launch processing modules one by one, according to data type
        datatype = tree.get_data_type()
        accomplished = True
        rank = 0
        for processing_module in PROCESSING_MODULES[datatype]:
            rank += 1
            # get comment and identifier
            (_, comment, _) = launch_processing_module([processing_module, "-i"])
            (_, identifier, _) = launch_processing_module([processing_module, "-u"])

            # run process
            cmd = [processing_module]
            cmd.extend(["-t", transaction_id, "-b", basepath, "-w", tempdir])
            logging.info("Running command %s", cmd)
            (returncode, stdoutdata, stderrdata) = launch_processing_module(
                cmd, stdindata="\n".join(str(x) for x in allfiles) + "\n", log=True
            )

            # write results to XML
            badfiles = stdoutdata.splitlines()
            logging.info("%d rejected files", len(badfiles))
            tree.add_process_result(
                identifier, comment, str(rank), str(returncode), sorted(badfiles)
            )
            tree.write(targetxml)

            # save output to log files
            basename = os.path.basename(processing_module)

            stderr_filename = "%s.%d-%s.stderr" % (transaction_id, rank, basename)
            with open(os.path.join(LOG_DIR, stderr_filename), "w") as f:
                f.write(stderrdata)

            stdout_filename = "%s.%d-%s.stdout" % (transaction_id, rank, basename)
            with open(os.path.join(LOG_DIR, stdout_filename), "w") as f:
                f.write(stdoutdata)

            # if fatal error
            if returncode == 128 or returncode not in (0, 1):
                accomplished = False
                break

        # if all modules were run successfully,
        if accomplished:
            tree.set_status("8")
            tree.set_comment(
                "Data sent to datacentre and data integration steps finished."
            )
            t = os.times()
            logging.info("total user+system time = %.1fs ", sum(t[0:4]))
            logging.info("transaction %s processing finished", transaction_id)
            if RUNMODE == "production":
                logging.info("deleting %s and %s", tempdir, basepath)
                try:
                    shutil.rmtree(tempdir)
                    shutil.rmtree(basepath)
                except PermissionError:
                    logging.exception("I was not able to delete the file %s")
                # raise information in Zabbix
                zabbix_packets.append(
                    ItemValue(ZABBIX_HOST, ZABBIX_KEY, transaction_id + " OK")
                )

        # else, restore original XML file to its original location
        # so transaction may be replayed if necessary
        else:
            tree.set_status("128")
            tree.set_comment(
                "Fatal error. Transaction aborted during integration steps."
            )
            shutil.copy(os.path.join(tempdir, xmlfilename), xmlfullpath)
            # log fatal error
            logging.error(
                "Fatal error in processing module : integration aborted. "
                "Original XML file has been restored to incoming directory."
            )
            # raise alarm in Zabbix
            # (be careful when modifying zabbix messages contents :
            # those are being parsed server-side)
            zabbix_packets.append(
                ItemValue(ZABBIX_HOST, ZABBIX_KEY, transaction_id + " ERROR")
            )
        # update XML file
        tree.write(targetxml)
        # appel à transaction_analyse
        logging.info(
            "Calling %s --f %s --Alarm > %s/transaction_analyse.txt",
            TRANSACTION_ANALYSE_EXE,
            targetxml,
            LOG_DIR,
        )
        os.system(
            f"{TRANSACTION_ANALYSE_EXE} --f {targetxml} --Alarm > "
            f"{LOG_DIR}/transaction_analyse.txt"
        )
    except Exception as err:
        logging.critical(traceback.format_exc())
        logging.critical(str(err))
        # raise alarm in Zabbix
        zabbix_packets.append(
            ItemValue(ZABBIX_HOST, ZABBIX_KEY, message + " Exception")
        )
        raise
    finally:
        sender = Sender(use_config=True, config_path=ZABBIX_CONF)
        response = sender.send(zabbix_packets)
        logging.info("Sent to zabbix : %s", response)


def on_message(_channel, _method, _, body):
    body = body.decode("utf-8")
    logging.info("Message received: %s", body)
    logging.debug("Consuming message %s", body)
    transaction_process(body)

    # dot not notify manually rabbitmq server that message has been handled
    # channel.basic_ack(delivery_tag=method.delivery_tag)


def signal_handler(channel, connection, signum, _frame):
    if signum == signal.SIGTERM:
        logging.info("Received SIGTERM signal. Terminating")
    elif signum == signal.SIGINT:
        logging.info("Received SIGINT signal. Terminating")
    logging.info("Disconnecting from %s", AMQP_SERVER)
    channel.stop_consuming()
    connection.close()
    logging.critical("integration worker quitting")
    sys.exit(0)


def main(args):
    log_setup(args.long)
    logging.info("integration worker started ------ RUNMODE %s ", RUNMODE)
    routing_key = ROUTING_KEY_LONG if args.long else ROUTING_KEY_DEFAULT
    credentials = pika.PlainCredentials(AMQP_USER, AMQP_PASSWORD)
    parameters = pika.ConnectionParameters(
        AMQP_SERVER,
        AMQP_PORT,
        AMQP_VHOST,
        credentials,
        heartbeat=AMQP_HEARTBEAT,
        # blocked_connection_timeout=AMQP_TIMEOUT,
    )
    connection = pika.BlockingConnection(parameters)
    logging.debug("Listening on %s:%s", AMQP_SERVER, routing_key)
    channel = connection.channel()
    channel.basic_qos(prefetch_count=1)
    channel.exchange_declare(exchange=EXCHANGE, exchange_type="direct", durable=True)
    channel.queue_declare(queue=routing_key, durable=True)
    channel.queue_bind(queue=routing_key, exchange=EXCHANGE, routing_key=routing_key)
    channel.basic_consume(
        queue=routing_key, on_message_callback=on_message, auto_ack=True
    )
    signal.signal(signal.SIGTERM, partial(signal_handler, channel, connection))
    signal.signal(signal.SIGINT, partial(signal_handler, channel, connection))
    channel.start_consuming()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Integration Worker")
    parser.add_argument(
        "-l",
        "--long",
        dest="long",
        action="store_true",
        help="Worker for long transactions",
    )
    args = parser.parse_args()
    main(args)

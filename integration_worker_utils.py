import logging
import operator
import os
import subprocess


def launch_processing_module(args, stdindata="", log=False):
    """launch external process as described by argument list 'args'"""
    if log:
        logging.info("launching %s ", " ".join(args))
    timestart = os.times()
    proc = subprocess.Popen(
        args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    (stdoutdata, stderrdata) = proc.communicate(input=stdindata.encode())
    timeend = os.times()
    # compute time delta
    t = list(map(operator.sub, timeend, timestart))
    if log:
        logging.info(
            "returncode=%d usertime=%.1fs systemtime=%.1fs walltime=%.1fs",
            proc.returncode,
            t[2],
            t[3],
            t[4],
        )
    return (proc.returncode, stdoutdata.decode(), stderrdata.decode())


def ls_files(rootdirectory):
    """
    return (as a list) all files found recursively in rootdirectory,
    paths are relative to rootdirectory.
    nota: strings are returned as UTF8 instances
    """
    allfiles = []
    for basedir, _subdirs, files in os.walk(rootdirectory):
        for filename in files:
            relativepath = os.path.relpath(basedir, rootdirectory)
            filerelativepath = os.path.join(relativepath, filename)
            allfiles.append(filerelativepath)
    return allfiles

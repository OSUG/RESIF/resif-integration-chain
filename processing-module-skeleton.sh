#!/bin/bash
#
# RESIF
# <<skeleton script for processing module implementation,
# replace this comment with your own description >>
#
# you must also edit :
#
#   MODULE_ID       (string)
#   INFO_STRING     (string)
#   signalhandler() (function)
#   processing()    (function)

# example interactive testing :
#
#   echo -e "file1\nfile2\nfile3" | RUNMODE=test ./processing-module-skeleton.sh -t YYY-111 -b /dir/data/ -w /dir/tmp/
#

# File history ---------------------------------
# [Jul 2013 - P.Volcke] empty script skeleton 
# [Aug 2013 - P.Volcke] added -u option
#

# Module ID
# fill this string with an unique module identifier 
# use [A-Za-z0-9] charset, don't use any CR/LF/TAB chars, no more than 8 chars.
MODULE_ID="!no id!"

# Information string
# fill this string with informations about what this module is doing.
# this string is returned when called with -i.
# use [A-Za-z0-9] charset, don't use any CR/LF/TAB chars, no more than 255 chars.
INFO_STRING="!!!no information!!!"
    
# Main processing function
function processing() {
    # add below your own code for processing :
    # stdin provides files list to process, you must return broken 
    # files list on stdout, and may write some log on stderr
    transactionID=$1
    basedir=$2
    workingdir=$3
    # loop on each file 
    while read filename; do
        fullpath=$basedir/$filename
        # ...
        # ...
    done
    # ...
    # ...
    # exit with correct return values (0, 1, 128)
    exit 128
}

# Signal handler function
# called when TERM signal is sent.
function signalhandler() {	
	# add here your own clean up actions and housekeeping.
	# ...
		
	# you must exit with 128
	exit 128
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# you should not need to edit anything below
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# this script relies on -u to disallow usage
# of uninitialised variable. Don't turn off!
set -u

#
# display help text
#
echousage() {
    scriptname="$(basename "$0")"
    cat > /dev/stderr << EOF
$scriptname
$INFO_STRING

PARAMETERS
    
    -u                  displays unique module identifier and exit
    -i                  displays human readble information about processing and exit
    -t TRANSACTION_ID   provides transaction ID
    -b BASEDIR          base directory of file names provided on stdin 
    -w WORKDIR          working directory to use for temporary I/O
    
    -t, -b, -w must be used altogether.

PRODUCTION / TEST MODES

Script behaviour depends on RUNMODE environnement variable (must be set),
either 'production' or 'test'.
   
RETURN VALUES

    0   procesing finished with success (next processing can be launched)
    1   processing finished with warning (next processing can be launched)
    128 fatal error (next processing cannot be launched)
     
SIGNALS

    TERM : will run cleanup actions, then exit with return code 128
        
EOF

    exit 1
}


# MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~

[ $# -eq 0 ] && echo "-h for help." && exit 1

while getopts "hiut:b:w:" flag; do
    case $flag in
        h)
            echousage
            ;;
        i)
            echo -n $INFO_STRING
            exit 0
            ;;
        u)
            echo -n $MODULE_ID
            exit 0
            ;;
        t)
            transactionID=$OPTARG
            ;;
        b)
            basedir=$OPTARG
            ;;
        w)
            workingdir=$OPTARG
            ;;
    esac
done

# check -t -b -w are set
trap "echo -t , -b , -w are mandatory altogether." EXIT
echo $transactionID > /dev/null
echo $basedir > /dev/null
echo $workingdir > /dev/null
trap - EXIT

# check directories exists
[[ ! -d $basedir ]] && echo "$basedir not found" && exit 128
[[ ! -d $workingdir ]] && echo "$workingdir not found" && exit 128

# check RUNMODE exists
trap "echo RUNMODE environnement variable must be set to 'test' or 'production'" EXIT
echo $RUNMODE > /dev/null
trap - EXIT

# set signal handler for TERM signal
trap signalhandler TERM

# launch processing 
processing $transactionID $basedir $workingdir


# Scripts pour piloter les étapes de l'intégration de données

  * integrationWorker.py : à installer sur la machine d'integration
  * processing-module-skeleton.sh :		modele pour ecriture de module de traitement
  * dummy-module-128.sh :			module "bidon" pour tests
  * ResifDataTransferTransaction.py :
  
## refresh_stationws.py

### Prérequis

python 3.11 ou plus

packages: pika, psycopg, kubernetes

### Configuration

Pour staging, toutes les valeurs par défaut sont en place (sauf les mots de passe rabbitmq et postgres)

Pour production:

``` sh
AMQP_HOST=resif-mq.u-ga.fr
AMQP_VHOST=production
AMQP_USER=integrator
AMQP_PASS=XXXXXXX
PG_RESIFINV_URI=postgres://resifinvprod@resif-pgprod.u-ga.fr:5432/resifInv-Prod
KUBE_CONFIG_FILE=~/.kube/config.yaml
K8S_POSTGRES_DEPLOYMENT=production-ws-station-postgres
K8S_NAMESPACE=production
```

Tout cela peut se mettre dans une description systemd d'un service.




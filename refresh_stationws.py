#!/usr/bin/env python3
# /// script
# requires-python = ">=3.12"
# dependencies = [
#     "pika",
#     "psycopg",
#     "kubernetes",
# ]
# ///

import logging
import time
import os
import json
from datetime import datetime, timedelta
import pika
import psycopg
from kubernetes import client, config
from kubernetes.client.rest import ApiException

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

min_refresh_interval = 600
delay_refresh = 300
last_refresh = datetime.now() - timedelta(seconds=delay_refresh+1)

rabbitmq_host = os.getenv("AMQP_HOST", "resif-mq.u-ga.fr")
rabbitmq_vhost = os.getenv("AMQP_VHOST", "staging")
rabbitmq_user = os.getenv("AMQP_USER", "integrator")
rabbitmq_pass = os.getenv("AMQP_PASS", "")
rabbitmq_postintegration_exchange = os.getenv("AMQP_POSTINTEGRATION_EXCHANGE", "post-integration")
queue = "refresh_station_ws"

pguri = os.getenv(
    "PG_RESIFINV_URI",
    "postgres://resifinvdev@resif-pgpreprod.u-ga.fr:5432/resifInv-Preprod",
)

k8s_config_file = os.getenv("KUBE_CONFIG_FILE", "~/.kube/config")
k8s_deployment = os.getenv("K8S_POSTGRES_DEPLOYMENT", "staging-ws-station-postgres")
k8s_namespace = os.getenv("K8S_NAMESPACE", "staging")


def should_run_now(integration_arrival):
    """
    Determine if the refresh steps should run now.
    Based on the integration arrival time (a DateTime object).

    If the refresh took already place in the last min_refresh_interval seconds,
    then we should not run now, return False.
    """
    last_run_age = integration_arrival - last_refresh
    last_run_age.total_seconds() >= min_refresh_interval


def refresh_sql_views():
    logger.info("Starting refresh materialized views")

    with psycopg.connect(pguri) as conn:
        with conn.cursor() as cur:
            cur.execute("""
            REFRESH MATERIALIZED VIEW channel_light;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_common;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_text;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_text;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_text;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_network_xml;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_station_xml;
            REFRESH MATERIALIZED VIEW CONCURRENTLY ws_channel_xml;
            """)
    logger.info("Done refreshing materialized views")
    True


def k8s_rollout_restart(deployment, namespace):
    logger.info("Starting k8s rollout restart")
    config.load_kube_config(k8s_config_file)
    # Enter name of deployment and "namespace"
    v1_apps = client.AppsV1Api()
    now = datetime.now()
    now = str(now.isoformat("T") + "Z")
    body = {
        "spec": {
            "template": {
                "metadata": {"annotations": {"kubectl.kubernetes.io/restartedAt": now}}
            }
        }
    }
    try:
        v1_apps.patch_namespaced_deployment(deployment, namespace, body, pretty="true")
    except ApiException as e:
        logger.exception(
            "Exception when calling AppsV1Api->read_namespaced_deployment_status: %s\n"
            % e
        )

    logger.info("Done restarting deployment")
    True

def notify_post_integration(msg):
    logger.info("Notify post integraiton queue with %s", msg)
    credentials = pika.PlainCredentials(rabbitmq_user, rabbitmq_pass)
    parameters = pika.ConnectionParameters(rabbitmq_host, 5672, rabbitmq_vhost, credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.basic_publish(exchange='post-integration',
                          routing_key='stationxml',
                          body=msg)

def on_message(ch, method, properties, body):
    global last_refresh
    logger.info(f"Received {body} with properties {properties.headers}")
    try:
        integration_arrival = datetime.fromtimestamp(int(properties.headers['timestamp']))
        logger.info(integration_arrival)
    except ValueError:
        logger.exception("Expected message body to be a unix epoch in seconds.")
        return 1

    logger.info("Should I do something?")
    # Discard the message if it was prior to our last refresh time.
    # Since we already refreshed all the views
    if integration_arrival < last_refresh:
        logger.info("Discard this message arrived at %s prior to last refresh %s", integration_arrival, last_refresh)
        notify_post_integration(body)
        return 0

    if not should_run_now(integration_arrival):
        # It's too early before previous refresh.
        logger.info(
            "Last refresh was too close. We wait %ss before launching it", delay_refresh
        )
        time.sleep(delay_refresh)

    logger.info(
        "Let's refresh the views and all"
    )
    last_refresh = datetime.now()
    refresh_sql_views()
    k8s_rollout_restart(k8s_deployment, k8s_namespace)
    notify_post_integration(message['msg'])
    return 0


credentials = pika.PlainCredentials(rabbitmq_user, rabbitmq_pass)
parameters = pika.ConnectionParameters(rabbitmq_host, 5672, rabbitmq_vhost, credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue=queue)

channel.basic_consume(queue=queue, auto_ack=True, on_message_callback=on_message)
logger.info("Start consuming queue on %s", queue)
channel.start_consuming()

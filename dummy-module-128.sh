#!/bin/bash

# a dummy module that always return 128

>&2 echo "Cette chaine d'integration n'est pas activée et renvoie donc systematiquement 128 pour stopper le processus dès son début."

exit 128

